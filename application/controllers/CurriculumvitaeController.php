<?php

class CurriculumvitaeController extends Zend_Controller_Action
{

	const REDIRECT_URL = '/curriculumvitae';
	
    public function init()
    {
        /* Initialize action controller here */
    	header('content-type: text/html; charset=utf8');  
    }

    public function indexAction()
    {
    	$param = $this->getRequest()->getParams();
    	$db = new Application_Model_DbTable_DbGlobalSelect();
    	$curriculum = $db->getLastCurriculum();
    	$this->view->ministerCurriculum = $curriculum;
    }  
}

<?php

class Advertising_Model_DbTable_Dbadvertising extends Zend_Db_Table_Abstract
{

    protected $_name = 'mini_company_adv';
    public static function getUserId(){
    	$session_user=new Zend_Session_Namespace('auth');
    	return $session_user->user_id;
    }
    static function getCurrentLang(){
    	$session_lang=new Zend_Session_Namespace('lang');
    	return $session_lang->lang_id;
    }
    public function getAllCompany($data){
    	$db=$this->getAdapter();
    	$lang = $this->getCurrentLang();
    	
    	$sql="SELECT c.id,c.com_name,c.com_phone,c.email,
			(SELECT province_kh_name FROM mini_province WHERE  province_id=c.province_id LIMIT 1) as province_name,
			c.status,
			(SELECT u.first_name FROM `rms_users` AS u WHERE u.id = c.user_id LIMIT 1) AS user_name
    	 	FROM `mini_company_adv` AS c WHERE com_name!='' ";
    	
		if(!empty($data['txt_search'])){
			$s_where = array();
			$s_search = addslashes(trim($data['txt_search']));
			$s_where[] = " com_name LIKE '%{$s_search}%'";
			$s_where[] = " com_phone LIKE '%{$s_search}%'";
			$s_where[] = " note LIKE '%{$s_search}%'";
			$s_where[] = " address LIKE '%{$s_search}%'";
			$sql.=' AND ('.implode(' OR ',$s_where).')';
		}
    	if ($data['province_id']>0){
    		$sql.=" AND c.`province_id`=".$data['province_id'];
    	}
    	$sql.=" ORDER BY c.id DESC";
    	return $db->fetchAll($sql);
    }
    function addadvertising($data){
    	$db = $this->getAdapter();
    	$db->beginTransaction();
    	try{
    		$dbg = new Application_Model_DbTable_DbGlobal();
    		$part= PUBLIC_PATH.'/companylogo/';
    		$name = $_FILES['photo']['name'];
    		$photo='';
    		if (!empty($name)){
    			$tem =explode(".", $name);
    			$new_image_name = date("Y").date("m").date("d").time().".".end($tem);
    			$photo = $dbg->resizeImase($_FILES['photo'], $part,$new_image_name);
    		}
	    	  $arr = array(
	    			'com_name'=>$data['company_name'],
					'com_phone'=>$data["phone"],
	    	  		'email'=>$data["email"],
	    			'address'=>$data["address"],
	    			'province_id'=>$data["province_id"],
	    			'note'=>$data["note"],
	    			'logo'=>$photo,
	    			'create_date'=>date("Y-m-d H:i:s"),
	    			'modify_date'=>date("Y-m-d H:i:s"),
	    			'user_id'=>$this->getUserId(),
	    			'status'=>1,
	    		);
	    	
	    	$this->insert($arr);
	    	$db->commit();
    	}catch(exception $e){
    		echo $e->getMessage();exit(); 
    		Application_Form_FrmMessage::message("Application Error");
    		Application_Model_DbTable_DbUserLog::writeMessageError($e->getMessage());
    		$db->rollBack();
    	}
	}
	function editCompany($data){
		$db = $this->getAdapter();
		$db->beginTransaction();
		try{
			$dbg = new Application_Model_DbTable_DbGlobal();
		$part= PUBLIC_PATH.'/companylogo/';
    		$name = $_FILES['photo']['name'];
    		$photo='';
    		if (!empty($name)){
    			$tem =explode(".", $name);
    			$new_image_name = date("Y").date("m").date("d").time().".".end($tem);
    			$photo = $dbg->resizeImase($_FILES['photo'], $part,$new_image_name);
    		}else{
    			$photo = $data['old_photo'];
    		}
	
			$arr = array(
					'com_name'=>$data['company_name'],
					'com_phone'=>$data["phone"],
					'email'=>$data["email"],
					'address'=>$data["address"],
					'province_id'=>$data["province_id"],
					'note'=>$data["note"],
					'logo'=>$photo,
					'user_id'=>$this->getUserId(),
					'status'=>$data["status"],
					'modify_date'=>date("Y-m-d H:i:s"),
	
			);
			$where = "id = ".$data['id'];
			$this->update($arr, $where);
			$db->commit();
		}catch(exception $e){
			Application_Form_FrmMessage::message("Application Error");
			Application_Model_DbTable_DbUserLog::writeMessageError($e->getMessage());
			$db->rollBack();
		}
	}
	function getCompanyById($id){
		$db= $this->getAdapter();
		$sql="SELECT * FROM `mini_company_adv` WHERE id =".$id;
		return $db->fetchRow($sql);
	}

}


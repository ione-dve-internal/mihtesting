<?php 
Class Advertising_Form_Frmadvertising extends Zend_Dojo_Form {
	protected $tr;
	public function init()
	{
		$this->tr = Application_Form_FrmLanguages::getCurrentlanguage();
	}
	public function FrmAddCompany($data=null){
		
		$request=Zend_Controller_Front::getInstance()->getRequest();
		$db = new Application_Model_DbTable_DbGlobal();
		
		$email = new Zend_Dojo_Form_Element_TextBox('email');
		$email->setAttribs(array(
				'dojoType'=>'dijit.form.TextBox',
				'class'=>'fullside',
				'required'=>'true',
		));
		
		$_company_name = new Zend_Dojo_Form_Element_ValidationTextBox('company_name');
		$_company_name->setAttribs(array(
				'dojoType'=>'dijit.form.ValidationTextBox',
				'class'=>'fullside',
				'required'=>'true',
				'placeholder'=>$this->tr->translate("Company Name")
		));
		$_phone = new Zend_Dojo_Form_Element_TextBox('phone');
		$_phone->setAttribs(array('dojoType'=>'dijit.form.TextBox','class'=>'fullside'));
		
		$_address = new Zend_Dojo_Form_Element_Textarea("address");
		$_address->setAttribs(array(
				'dojoType'=>'dijit.form.Textarea',
				'class'=>'fullside',
				'style'=>'width:100%;min-height:60px; font-size:inherit; font-family:Kh Battambang'
		));
	
		$_note = new Zend_Dojo_Form_Element_Textarea("note");
		$_note->setAttribs(array(
				'dojoType'=>'dijit.form.Textarea',
				'class'=>'fullside ckeditor',
				'style'=>'width:100%; min-height:60px; height:200px; font-size:inherit; font-family:Kh Battambang'
		));
		
		
		$_status=  new Zend_Dojo_Form_Element_FilteringSelect('status');
		$_status->setAttribs(array('dojoType'=>'dijit.form.FilteringSelect','class'=>'fullside',));
		$_status_opt = array(
				1=>$this->tr->translate("ACTIVE"),
				0=>$this->tr->translate("DACTIVE"));
		$_status->setMultiOptions($_status_opt);
		
		$_province = new Zend_Dojo_Form_Element_FilteringSelect('province_id');
		$_province->setAttribs(array(
				'dojoType'=>'dijit.form.FilteringSelect',
				'class'=>'fullside',
		));
		
		$rows =  $db->getAllProvince();
		$options=array($this->tr->translate("SELECT_PROVINCE"));
		if(!empty($rows))foreach($rows AS $row) $options[$row['id']]=$row['name'];
		$_province->setMultiOptions($options);
		
		$id = new Zend_Form_Element_Hidden('id');
		
		if($data!=null){
			$_company_name->setValue($data['com_name']);
			$_phone->setValue($data['com_phone']);
			$email->setValue($data['email']);
			$_address->setValue($data['address']);
			$_note->setValue($data['note']);
			$_status->setValue($data['status']);
			$id->setValue($data['id']);
		}
		$this->addElements(array(
				$id,
				$_company_name,$email,
				$_phone,
				$_address,
				$_note,
				$_status,$_province
				));
		return $this;
		
	}	
	
}